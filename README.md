# generate-ics

## References

- [ics.py](https://github.com/ics-py/ics-py)
- [python-docx](https://python-docx.readthedocs.io/en/latest/)

## Notes

- https://github.com/collective/icalendar (Python package)
