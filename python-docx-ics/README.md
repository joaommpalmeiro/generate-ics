# Create an iCalendar file from a table in a Word document

## Development

```bash
pipenv install --dev
```

```bash
pipenv shell
```

```bash
python script.py
```

```bash
isort --profile black script.py && black script.py
```

## Notes

- `pipenv --python 3.7`
- `pipenv install python-docx ics && pipenv install --dev black isort`
