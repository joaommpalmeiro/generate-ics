from pathlib import Path
from typing import Dict

import arrow
from docx import Document
from ics import Calendar, Event

DOCX_DOCUMENT: Path = Path("Cronograma - 2023-01-06T170741.765.docx")
TABLE_INDEX: int = 1
DT_FORMAT: str = "dddd, D [de] MMMM [de] YYYY HH:mm"
CAL_FILENAME: str = "course"

COLS_TO_INDEX: Dict[str, int] = {
    "Data da Sessão": 0,
    "Hora de início": 1,
    "Hora de fim": 2,
    "Módulo": 3,
    "Formador(es)": 4,
}

if __name__ == "__main__":
    # import locale
    # print(len(locale.locale_alias))
    # print(locale.locale_alias)

    # On Ubuntu/WSL, run these commands first if changing the locale raises an error (https://askubuntu.com/a/227513):
    # sudo locale-gen "pt_PT"
    # locale -a
    # locale.setlocale(locale.LC_TIME, "pt_PT")

    # https://stackoverflow.com/a/46618756
    # https://python-docx.readthedocs.io/en/latest/api/table.html
    # https://python-docx.readthedocs.io/en/latest/api/table.html#docx.table._Cell
    document = Document(DOCX_DOCUMENT)
    # print(document.tables)
    # print(len(document.tables[0].rows))
    table = document.tables[TABLE_INDEX]

    # https://icspy.readthedocs.io/en/v0.7.2/#create-a-new-calendar-and-add-events
    # https://github.com/ics-py/ics-py/tree/v0.7.2#quickstart
    # https://arrow.readthedocs.io/en/latest/
    # https://day.js.org/docs/en/display/format
    # https://arrow.readthedocs.io/en/latest/api-guide.html#arrow.locales.PortugueseLocale
    # https://icspy.readthedocs.io/en/stable/api.html#calendar
    # https://icspy.readthedocs.io/en/stable/api.html#event
    c = Calendar()

    # `[1:]` to ignore the table header.
    for row in table.rows[1:]:
        cells = row.cells

        start_dt_str = f"{cells[COLS_TO_INDEX['Data da Sessão']].text} {cells[COLS_TO_INDEX['Hora de início']].text}"
        end_dt_str = f"{cells[COLS_TO_INDEX['Data da Sessão']].text} {cells[COLS_TO_INDEX['Hora de fim']].text}"
        # print(start_dt_str)

        # https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes
        # from datetime import datetime
        # start_dt = datetime.strptime(start_dt_str, "%A, %d de %B de %Y %H:%M")
        # print(start_dt)

        # https://github.com/arrow-py/arrow/blob/master/arrow/locales.py
        start_dt = arrow.get(start_dt_str, DT_FORMAT, locale="pt-pt")
        end_dt = arrow.get(end_dt_str, DT_FORMAT, locale="pt-pt")
        # print(start_dt)

        tutor_names = cells[COLS_TO_INDEX["Formador(es)"]].text.split(";")
        # print(repr(cells[COLS_TO_INDEX["Formador(es)"]].text))
        # print(tutor_names)

        prefix_description = "Formador" if len(tutor_names) == 1 else "Formadores"
        description = f"{prefix_description}: {' + '.join(tutor_names)}"
        # print(description)

        # https://icspy.readthedocs.io/en/stable/api.html#ics.event.Event.begin
        e = Event(
            name=cells[COLS_TO_INDEX["Módulo"]].text,
            begin=start_dt,
            end=end_dt,
            description=description,
        )

        c.events.add(e)

    # print(c)
    # print(c.serialize())

    # https://icspy.readthedocs.io/en/v0.7.2/#export-a-calendar-to-a-file
    with open(f"{CAL_FILENAME}.ics", "w") as f:
        f.writelines(c.serialize_iter())

    print("All done!")
